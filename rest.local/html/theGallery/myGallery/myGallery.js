document.addEventListener("DOMContentLoaded", function() { 
    const uploader = document.querySelector("uploader");
    if(!uploader) throw "Uploader-container not found";
    
    const addPictureButton = document.getElementById("addPicture");
    if(!addPictureButton)throw "addPictureButton not found";
    addPictureButton.addEventListener("click", addPictureClick)

    //loadGallery();
	//initPaginator();
	initFilter();
	initLangSwitch();
});

function initLangSwitch() {
	const langSwitch = document.getElementById("langSwitch");
	const langSelect = document.getElementById("langSelect");
	const setLang = document.getElementById("setLang");
	if(!langSwitch || !langSelect || !setLang)
		throw "initLangSwitch - element(s) location error";
	fetch("/api/gallery?langs").then(r=>r.json())
	.then(j => {
		j.push("all");
		for(let lang of j) {
			let opt = document.createElement("option");
			opt.value = lang;
			opt.innerText = lang;
			langSelect.appendChild(opt);
		}
		
		setLang.onclick = langChange;
	});
}
function langChange() {
	const opt = document.querySelector("#langSelect option:checked");
	if(!opt) {
		alert("Select lang before switching");
		return;
	}
	loadGallery({'lang': opt.value});
}

function initFilter() {
	const applyFilter = document.querySelector("#applyFilter");
	if(!applyFilter) throw "applyFilter button not found";
	applyFilter.addEventListener("click", applyFilterClick);
}

function applyFilterClick() {
	const datePicker = document.querySelector("#datePicker");
	if(!datePicker) throw "datePicker button not found";
	const date = datePicker.value;
	if(date.length == 0) {
		alert("Select date to filter");
		return;
	}
	loadGallery({'date': date});
}

function addPictureClick(e) {
    const picFile = document.querySelector("[name=pictureFile]");
    if(!picFile) throw "File picture not found";

    if(picFile.files.length == 0) {
        alert("Выберите файл!");
        return;
    }

    const fd = new FormData();
    fd.append("pictureFile", picFile.files[0]);
    for( let elem of [
		"pictureDescriptionUk",
		"pictureDescriptionEn",
		"pictureDescriptionRu"
		] ) {
			var picDescr = e.target.parentNode.querySelector(`[name=${elem}]`);
			if(!picDescr) throw `${elem} not found`;
			// console.log(picDescr.value);
			fd.append(elem, picDescr.value);
		}

    fetch("/api/gallery",{
        method: "post",
        headers: {

        },
        body: fd
    })
    .then(r=>r.text())
    .then(console.log);
}

function loadGallery() {
	fetch("/api/gallery")
	.then(r=>r.text())
	.then(showGallery);
}

function showGallery(t) {
	const cont = document.querySelector("gallery");
	if(!cont) throw "Gallery container not found";
	try {
		var j = JSON.parse(t);
	} 
	catch {
		console.log("JSON parse error");
		console.log(t);
		return;
	}
	const picTpl = `
		<div class='picture'>
			<img src='/pictures/{{filename}}' />
			<b>{{moment}}</b>
			<p>{{descr}}</p>
		</div>
	`;
	var contHTML = "";
	for(let pic of j.data) {
		contHTML += picTpl
			.replace( "{{filename}}", pic.filename)
			.replace( "{{moment}}", pic.moment)
			.replace( "{{descr}}", pic.descr);
	}
	cont.innerHTML = contHTML;
	cont.setAttribute("pageNumber", j.meta.page);
    window.lastPageNumber.innerText = j.meta.lastPage;
}

document.addEventListener("DOMContentLoaded", () => {
    // создаем объект galleryWindow
    window.galleryWindow = {
        state: {},
        changeState: s => {
            if(typeof s == 'undefined') return;
            const state = window.galleryWindow.state;

            if(typeof s["pageNumber"] != 'undefined') state.pageNumber = s["pageNumber"];

            var url = "/api/gallery?page=" + state.pageNumber;
            fetch(url)
            .then(r => r.text())
            .then(result => {
                //console.log(document.cookie);
                //const j = JSON.parse(result);
                const cont = document.querySelector("gallery");
                if(!cont) throw "Gallery container not found";
                try {
                    var j = JSON.parse(result);
                } 
                catch {
                    console.log("JSON parse error");
                    return;
                }
                const picTpl = `
                    <div class='picture'>
                        <img src='/pictures/{{filename}}' />
                        <b>{{moment}}</b>
                        <p>{{descr}}</p>
                    </div>
                `;
                var contHTML = "";
                for(let pic of j.data) {
                    contHTML += picTpl
                        .replace( "{{filename}}", pic.filename)
                        .replace( "{{moment}}", pic.moment)
                        .replace( "{{descr}}", pic.descr);
                }
                cont.innerHTML = contHTML;
                window.galleryWindow.state.pageNumber = j.meta.page;
                window.galleryWindow.state.lastPage = j.meta.lastpage;
                document.dispatchEvent(new CustomEvent(
                    "galleryWindowChange",
                    {detail: window.galleryWindow.state}
                ));
            })
        }
    };
    window.galleryWindow.changeState({pageNumber: 1});
} );

document.addEventListener("DOMContentLoaded", ()=> {
    const prevPageButton = document.getElementById("prevPageButton");
    if(!prevPageButton) throw "Pagination: prebPageButton not found";
    const nextPageButton = document.getElementById("nextPageButton");
    if(!nextPageButton) throw "Pagination: prebPageButton not found";

    prevPageButton.addEventListener("click", prevPageButtonClick);
    nextPageButton.addEventListener("click", nextPageButtonClick);
})

function prevPageButtonClick(e) {
    var page = window.galleryWindow.state.pageNumber;
    if(page > 1) {
        page--;
        window.nextPageButton.removeAttribute("disabled");
        
        window.galleryWindow.changeState({pageNumber: page})
    }
}

function nextPageButtonClick(e) {
    var page = window.galleryWindow.state.pageNumber;
    if(page < window.galleryWindow.state.lastPage) {
        page++;
        window.galleryWindow.changeState({pageNumber: page})
    } e.target.setAttribute("disabled", true);
}

function lastPageNumberListener(e) {
    window.lastPageNumber.innerText = e.detail.lastPage;
}
document.addEventListener("galleryWindowChange", lastPageNumberListener);

function currentPageNumberListener(e) {
    window.currentPageNumber.innerText = e.detail.pageNumber;
}
document.addEventListener("galleryWindowChange", currentPageNumberListener);

function buttonDisabler() {
    if(window.galleryWindow.state.pageNumber == window.galleryWindow.state.lastPage) window.nextPage.setAttribute("disabled", "disabled");
    else window.nextPage.removeAttribute("disabled");

    if(window.galleryWindow.state.pageNumber == 1) window.prevPage.setAttribute("disabled", "disabled");
    else window.prevPage.removeAttribute("disabled");
}
document.addEventListener("galleryWindowChange", buttonDisabler);