<?php

$dtl = empty($_GET['dtl']) 
    ? "home" 
    : trim($_GET['dtl']);
    
if( $dtl == 'gallery' or $dtl == 'locale' )	{
    include 'theGallery/' . $dtl . "_api.php" ;
    exit ;
}
include "api.php";

function sendError($err = 400, $msg = "Bad request") {
    $code = $err;
    if(is_string($err)) $msg = $err;
    if(is_array($err)) {
        if(isset($err['code'])) $code = $err['code'];
        if(isset($err['text'])) $code = $err['text'];
    }
    http_response_code($code);
    echo $msg;
    exit;
}