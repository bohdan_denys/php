document.addEventListener("DOMContentLoaded", function() {
    const testGetButton = document.getElementById("testGetButton");
    if(!testGetButton)throw "testGetButton not found";
    else testGetButton.onclick = testGet;

    const testPostButton = document.getElementById("testPostButton");
    if(!testPostButton)throw "testPostButton not found";
    else testPostButton.onclick = testPost;

    const testPutButton = document.getElementById("testPutButton");
    if(!testPutButton)throw "testPutButton not found";
    else testPutButton.onclick = testPut;

    const testDeleteButton = document.getElementById("testDeleteButton");
    if(!testDeleteButton)throw "testDeleteButton not found";
    else testDeleteButton.onclick = testDelete;

    const filePostButton = document.getElementById("filePostButton");
    if(!filePostButton)throw "filePostButton not found";
    else filePostButton.onclick = filePost;

    const filePutButton = document.getElementById("filePutButton");
    if(!filePutButton)throw "filePutButton not found";
    else filePutButton.onclick = filePut;

    const localeUaButton = document.getElementById("localeUaButton");
	if(!localeUaButton) throw "localeUaButton not found";
	else localeUaButton.onclick = localeUaButtonClick;
	
	const localeEnButton = document.getElementById("localeEnButton");
	if(!localeEnButton) throw "localeEnButton not found";
	else localeEnButton.onclick = localeEnButtonClick;

    const localeRuButton = document.getElementById("localeRuButton");
    if(!localeRuButton) throw "localeRuButton not found";
    else localeRuButton.onclick = localeRuButtonClick;
});

function localeEnButtonClick() {
	fetch("/api/locale",{
		method: "get",
		headers: {
			"Locale": "en"
		}
	})
	.then(r=>r.text())
	.then(t=>{
		out.innerHTML = t;
	});
}

function localeUaButtonClick() {
	fetch("/api/locale",{
		method: "get",
		headers: {
			"Locale": "ua"
		}
	})
	.then(r=>r.text())
	.then(t=>{
		out.innerHTML = t;
	});
}

function localeRuButtonClick() {
    fetch("/api/locale", {
        method: "get",
        headers: {
            "Locale": "ru"
        }
    })
    .then(r=>r.text())
    .then(t=>{
        out.innerHTML = t;
    });
}

function testGet() {
    const out = document.getElementById("out");
    if(!out) throw "testGetButton not found";

    fetch("/api?x=10&y=21")
        .then(r=>r.text())
        .then(r=>{
            out.innerText = r;
        })
}

function testPost() {
    const out = document.getElementById("out");
    if(!out) throw "testGetButton not found";

    fetch("/api", {
        method: "post",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            "x": 10,
            "y": 15
        })
    })
    .then(r=>r.text())
    .then(r=>{
        out.innerText = r;
    })
}

function testPut() {
    out.innerText = "PUT works";

    fetch("/api", {
        method: "put",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            "x": 18,
            "y": 24
        })
    })
    .then(r=>r.text())
    .then(r=>{
        out.innerText = r;
    })
}

function testDelete() {
    out.innerText = "DELETE works";

    fetch("/api", {
        method: "delete",
        headers: {
            "Content-Type": "application/json"
        },
        body: JSON.stringify({
            "x": 21,
            "y": 93
        })
    })
    .then(r=>r.text())
    .then(r=>{
        out.innerText = r;
    })
}
 
function filePost() {
    const fileInput = document.querySelector("input[name=userFile]");
    if(!fileInput) throw "input[name=userFile] not found";
    if(fileInput.files.length == 0) {
        alert("Select a file!");
        return;
    }
    const fd = new FormData();
    fd.append("userFile", fileInput.files[0]);
    fetch("/api/file", {
        method: "post",
        body: fd
    }).then(r=>r.text()).then(t=>{
        const out = document.getElementById("out");
        if(!out) throw "out not found";
        out.innerHTML = t;
    })
}

function filePut() {
    const fileInput = document.querySelector("input[name=userFile]");
    if(!fileInput) throw "input[name=userFile] not found";
    if(fileInput.files.length == 0) {
        alert("Select a file!");
        return;
    }
    const fd = new FormData();
    fd.append("userFile", fileInput.files[0]);
    fetch("/api/file", {
        method: "put",
        body: fd
    }).then(r=>r.text()).then(t=>{
        const out = document.getElementById("out");
        if(!out) throw "out not found";
        out.innerHTML = t;
    })
}