<?php


$method = strtoupper($_SERVER['REQUEST_METHOD']);

switch($method) {
    case 'GET': doGet(); break;
    case 'POST': 
        if($dtl == "file")
            doPostFile();
        else
            doPost();
        break;
    case 'PUT': 
        if($dtl == "file")
            doPutFile();
        else
            doPut();
        break;
    case 'DELETE': doDelete(); break;
    case 'PATCH': doPatch(); break;

    default: sendError(418, "");
}

function doGet() {
    if(checkXY($_GET['x'], $_GET['y'])) {
        $x = $_GET['x'];
        $y = $_GET['y'];
    }
    echo "GET API works with x=$x, y=$y";
}

function doPost() {
    
    $contentType = strtolower( trim ($_SERVER[ 'CONTENT_TYPE' ]));
    if($contentType == "application/json") {
        $body = file_get_contents ("php://input");
        $date = json_decode($body, true);
        if(JSON_ERROR_NONE !== json_last_error()) {
            sendError(412,"JSON parse error");
        }
        if(checkXY($date['x'], $date['y']))
        $x = $date['x'];
        $y = $date['y'];
         
        echo "POST API works with x=$x, y=$y";
    } else if($contentType == "application/x-www-form-urlencoded") {
        var_dump($_POST);
    } else {
        send415();
    }
    //var_dump($_POST);
    //echo "POST API works";
}

function doPut() {
    $body = file_get_contents("php://input");
    $date = json_decode($body, true);
    if(JSON_ERROR_NONE !== json_last_error()) {
        sendError(412, "JSON parse error");
    }
    if(checkXY($date['x'], $date['y'])) {
        $x = $date['x'];
        $y = $date['y'];
    }
    echo "PUT API works with x=$x, y=$y";
}

function doDelete() {
    $body = file_get_contents("php://input");
    $date = json_decode($body, true);
    if(JSON_ERROR_NONE !== json_last_error()) {
        sendError(412, "JSON parse error");
    }
    if(checkXY($date['x'], $date['y'])) {
        $x = $date['x'];
        $y = $date['y'];
    }
    echo "DELETE API works with x=$x, y=$y";
}

function doPostFile() {
    // echo "<pre>";
    // print_r($_FILES);
    // print_r($_SERVER);
    if(empty($_FILES['userFile'])) {
        sendError(412, "File must be attached");
    }
    if($_FILES['userFile']['error'] != 0) {
        sendError(500, "Error uploading file");
    }
    if($_FILES['userFile']['size'] == 0) {
        sendError(412, "Empty file not allowed");
    }
    $countName = 1;
    $fileName = $_FILES['userFile']['name'];
    while(file_exists("uploads/" . $fileName)) {
        $pos = strpos($_FILES['userFile']['name'], '.');

        $fileName = substr_replace($_FILES['userFile']['name'], " ($countName)", $pos, 0);
        //$fileName = $countName . $_FILES['userFile']['name'];
        $countName++;
    }
    if(move_uploaded_file(
        $_FILES['userFile']['tmp_name'],
        "uploads/" . $fileName))
    {
        echo "Upload Ok";
    }
    else {
        sendError("Upload failed");
    }
}

function doPutFile() {
    echo "<pre>";
    print_r($_FILES);
    print_r($_SERVER);
}

function doPatch() {
    echo "<pre>";
    print_r($_FILES);
    print_r($_SERVER);
}

function send412($msg = "") {
    http_response_code(412);
    echo $msg;
    exit;
}

function send415($msg = "") {
    http_response_code(415);
    if(is_null($msg)) $msg = "Unsupported media type";
    echo $msg;
    exit;
}

function send418() {
    http_response_code(418);
    echo "API method does not support";
    exit;
}

function send500($msg = "") {
    http_response_code(500);
    echo $msg;
    exit;
}

function checkXY($x, $y) {
    if( ! isset($x))
    {
        send412("Parameter 'x' required");
    } 
    if( ! is_numeric($x)) {
        send412("Parameter 'x' should be numeric");
    }
    if( ! isset($y))
    {
        send412("Parameter 'y' required");
    } 
    if( ! is_numeric($y)) {
        send412("Parameter 'y' should be numeric");
    }
    
    return true;
}

//echo "<pre>"; print_r($_SERVER);